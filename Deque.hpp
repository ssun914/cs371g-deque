// -------
// Deque.h
// --------

#ifndef Deque_h
#define Deque_h

// --------
// includes
// --------

#include <algorithm>        // copy, equal, lexicographical_compare, max, swap
#include <cassert>          // assert
#include <initializer_list> // initializer_list
#include <iterator>         // bidirectional_iterator_tag
#include <memory>           // allocator
#include <stdexcept>        // out_of_range
#include <utility>          // !=, <=, >, >=

// -----
// using
// -----

using std::rel_ops::operator!=;
using std::rel_ops::operator<=;
using std::rel_ops::operator>;
using std::rel_ops::operator>=;

// -------
// destroy
// -------

template <typename A, typename BI>
BI my_destroy (A& a, BI b, BI e) {
    while (b != e) {
        --e;
        a.destroy(&*e);
    }
    return b;
}

// ------------------
// uninitialized_copy
// ------------------

template <typename A, typename II, typename BI>
BI my_uninitialized_copy (A& a, II b, II e, BI x) {
    BI p = x;
    try {
        while (b != e) {
            a.construct(&*x, *b);
            ++b;
            ++x;
        }
    }
    catch (...) {
        my_destroy(a, p, x);
        throw;
    }
    return x;
}

// ------------------
// uninitialized_fill
// ------------------

template <typename A, typename BI, typename T>
void my_uninitialized_fill (A& a, BI b, BI e, const T& v) {
    BI p = b;
    try {
        while (b != e) {
            a.construct(&*b, v);
            ++b;
        }
    }
    catch (...) {
        my_destroy(a, p, b);
        throw;
    }
}

// --------
// my_deque
// --------

template <typename T, typename A = std::allocator<T>>
class my_deque {
    // -----------
    // operator ==
    // -----------

    /**
     * @param lhs is a const my_deque passed in by reference
     * @param rhs is another const my_deque passed in by reference
     *
     * this function compares the two const deques, lhs and rhs, for equality
     *
     */
    friend bool operator == (const my_deque& lhs, const my_deque& rhs) {
        if(lhs.size() == 0 && rhs.size() == 0) {
            return true;
        }
        if(lhs.size() == rhs.size()) {
            if(std::equal(lhs.begin(), lhs.end(), rhs.begin())) { // you must use std::equal()
                return true;
            }
        }
        return false;
    }

    // ----------
    // operator <
    // ----------

    /**
     * @param lhs is a const my_deque passed in by reference
     * @param rhs is a const my_deque passed in by reference
     *
     * this function lexiographically compares the two const deques, lhs and rhs, and returns true if lhs < rhs
     */
    friend bool operator < (const my_deque& lhs, const my_deque& rhs) {
        return std::lexicographical_compare(lhs.begin(), lhs.end(), rhs.begin(), rhs.end()); // you must use std::lexicographical_compare()
    }

    // ----
    // swap
    // ----

    /**
     * swaps the contents of x and y
     */
    friend void swap (my_deque& x, my_deque& y) {
        x.swap(y);
    }

public:
    // ------
    // usings
    // ------

    // you must use this allocator for the inner arrays
    using allocator_type  = A;
    using value_type      = typename allocator_type::value_type;

    using size_type       = typename allocator_type::size_type;
    using difference_type = typename allocator_type::difference_type;

    using pointer         = typename allocator_type::pointer;
    using const_pointer   = typename allocator_type::const_pointer;

    using reference       = typename allocator_type::reference;
    using const_reference = typename allocator_type::const_reference;

    // you must use this allocator for the outer array
    using allocator_type_2 = typename A::template rebind<pointer>::other;

private:
    // ----
    // data
    // ----
    const size_type CONTAINER_SIZE = 10;

    allocator_type_2 _outer_array_allocator;
    allocator_type _inner_array_allocator;

    pointer* _outer_start;
    pointer* _outer_end;

    pointer* _outer_used_start;
    pointer* _outer_used_end;

    pointer _used_start;
    pointer _used_end;

    // number of elements in deque
    size_type _size;

    // number of inner arrays
    size_type _num_containers;

    // number inner arrays * CONTAINER_SIZE (roughly)
    size_type _capacity;

private:
    // -----
    // valid
    // -----

    bool valid () const {
        if(_size < 0) {
            return false;
        }
        if(_capacity < 0) {
            return false;
        }
        if(_outer_start > _outer_end || _outer_used_start > _outer_used_end) {
            return false;
        }
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * check equality
         */
        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return (lhs._current_deque == rhs._current_deque) && (lhs._idx == rhs._idx);
        }

        /**
         * check non equality
         */
        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * add to iterator to move forward
         */
        friend iterator operator + (iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * move backwards in iterator
         */
        friend iterator operator - (iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::pointer;
        using reference         = typename my_deque::reference;

    private:
        // ----
        // data
        // ----

        size_type _idx;
        my_deque* _current_deque;

    private:
        // -----
        // valid
        // -----

        bool valid () const {
            if(_current_deque == nullptr) {
                return false;
            }
            if(_idx < 0 || _idx > _current_deque->_capacity) {
                return false;
            }
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        iterator(my_deque& d): _current_deque(d) {}

        /**
         * iterator constructor
         */
        iterator (my_deque* d = nullptr, size_type i = 0) : _current_deque(d), _idx(i) {
            assert(valid());
        }

        iterator             (const iterator&) = default;
        ~iterator            ()                = default;
        iterator& operator = (const iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * get value at iterator spot
         */
        reference operator * () const {
            return _current_deque->operator[](_idx);
        }

        // -----------
        // operator ->
        // -----------

        /**
         * iterator arrow
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * increment iterator
         */
        iterator& operator ++ () {
            ++_idx;
            assert(valid());
            return *this;
        }

        /**
         * increment iterator
         */
        iterator operator ++ (int) {
            iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * decrement iterator
         */
        iterator& operator -- () {
            --_idx;
            assert(valid());
            return *this;
        }

        /**
         * decrement iterator
         */
        iterator operator -- (int) {
            iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * increment iterator by d
         */
        iterator& operator += (difference_type d) {
            _idx += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * decrement iterator by d
         */
        iterator& operator -= (difference_type d) {
            _idx -= d;
            assert(valid());
            return *this;
        }

        // accessor methods
        // size_type get_idx(){
        //     return _idx;
        // }

        // my_deque* get_current_deque(){
        //     return _current_deque;
        // }
    };

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        /**
         * iterator equality
         */
        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (lhs._current_deque == rhs._current_deque) && (lhs._idx == rhs._idx);
        }

        /**
         * iterator inequality
         */
        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

        // ----------
        // operator +
        // ----------

        /**
         * increment iterator by rhs
         */
        friend const_iterator operator + (const_iterator lhs, difference_type rhs) {
            return lhs += rhs;
        }

        // ----------
        // operator -
        // ----------

        /**
         * decrement iterator by rhs
         */
        friend const_iterator operator - (const_iterator lhs, difference_type rhs) {
            return lhs -= rhs;
        }

    public:
        // ------
        // usings
        // ------

        // this requires a weaker iterator than the real deque provides
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type        = typename my_deque::value_type;
        using difference_type   = typename my_deque::difference_type;
        using pointer           = typename my_deque::const_pointer;
        using reference         = typename my_deque::const_reference;

    private:
        // ----
        // data
        // ----

        // <your data>
        size_type _idx;
        const my_deque* _current_deque;

    private:
        // -----
        // valid
        // -----


        bool valid () const {
            if(_current_deque == nullptr) {
                return false;
            }
            if(_idx < 0 || _idx > _current_deque->_capacity) {
                return false;
            }
            return true;
        }

    public:
        // -----------
        // constructor
        // -----------

        /**
         * const iterator constructor
         */

        const_iterator (const my_deque* d = nullptr, size_type i = 0) : _current_deque(d), _idx(i) {
            // <your code>
            assert(valid());
        }

        const_iterator             (const const_iterator&) = default;
        ~const_iterator            ()                      = default;
        const_iterator& operator = (const const_iterator&) = default;

        // ----------
        // operator *
        // ----------

        /**
         * dereference iterator ptr
         */
        reference operator * () const {
            return _current_deque->operator[](_idx);
        }

        // -----------
        // operator ->
        // -----------

        /**
         * arrow for iterator
         */
        pointer operator -> () const {
            return &**this;
        }

        // -----------
        // operator ++
        // -----------

        /**
         * increment iterator
         */
        const_iterator& operator ++ () {
            ++_idx;
            assert(valid());
            return *this;
        }

        /**
         * increment iterator
         */
        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator --
        // -----------

        /**
         * decrement iterator
         */
        const_iterator& operator -- () {
            // <your code>
            --_idx;
            assert(valid());
            return *this;
        }

        /**
         * decrement iterator
         */
        const_iterator operator -- (int) {
            const_iterator x = *this;
            --(*this);
            assert(valid());
            return x;
        }

        // -----------
        // operator +=
        // -----------

        /**
         * increment it by d
         */
        const_iterator& operator += (difference_type d) {
            _idx += d;
            assert(valid());
            return *this;
        }

        // -----------
        // operator -=
        // -----------

        /**
         * decrement it by d
         */
        const_iterator& operator -= (difference_type d) {
            _idx -= d;
            assert(valid());
            return *this;
        }
    };


public:
    // ------------
    // constructors
    // ------------

    my_deque () {
        // allocate one block
        _outer_start = _outer_array_allocator.allocate(1);
        _outer_end = _outer_start + 1;
        _outer_used_end = _outer_end;
        _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
        _outer_start[0] = _used_start;
        _outer_used_start = _outer_start;
        _used_end = _used_start;
        _size = 0;
        _num_containers = 1;
        _capacity = CONTAINER_SIZE;
    };

    /**
     * construct deque from size, duplicate value_type() until filled
     */
    explicit my_deque (size_type s) {
        if(s == 0) {
            // allocate one block
            _outer_start = _outer_array_allocator.allocate(1);
            _outer_end = _outer_start;
            _outer_used_end = _outer_end;
            _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
            _outer_start[0] = _used_start;
            _outer_used_start = _outer_start;
            _used_end = _used_start;
            _size = 0;
            _num_containers = 1;
            _capacity = CONTAINER_SIZE;
        } else if(s > 0) {
            // set size
            _size = s;

            // set num_containers
            if((s % CONTAINER_SIZE) > 0) { // indicates that we have a partial container
                _num_containers = (s / CONTAINER_SIZE) + 1;
            } else {
                _num_containers = (s / CONTAINER_SIZE);
            }

            // set the max capacity of the outer arrays
            _capacity = _num_containers * CONTAINER_SIZE;

            // allocate a fat chunk
            _outer_start = _outer_array_allocator.allocate(_num_containers);

            for(int i = 0; i < _num_containers; ++i) {
                if(i == 0) {
                    _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
                    _outer_start[0] = _used_start;
                } else {
                    _outer_start[i] = _inner_array_allocator.allocate(CONTAINER_SIZE);
                }
            }
            _used_end = _used_start + _size;

            _outer_end = _outer_start + _num_containers;
            _outer_used_start = _outer_start;
            _outer_used_end = _outer_end;

            // fill
            my_uninitialized_fill(_inner_array_allocator, begin(), end(), value_type());

        } else {
            throw std::out_of_range("Please input a valid size for deque.");
        }
    }

    /**
     * construct from size and duplicate v until filled
     */
    my_deque (size_type s, const_reference v) {
        if(s == 0) {
            // allocate one block
            _outer_start = _outer_array_allocator.allocate(1);
            _outer_end = _outer_start + 1;
            _outer_used_end = _outer_end;
            _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
            _outer_start[0] = _used_start;
            _outer_used_start = _outer_start;
            _used_end = _used_start;
            _size = 0;
            _num_containers = 1;
            _capacity = CONTAINER_SIZE;
        } else if(s > 0) {
            // set size
            _size = s;

            // set num_containers
            if((s % CONTAINER_SIZE) > 0) { // indicates that we have a partial container
                _num_containers = (s / CONTAINER_SIZE) + 1;
            } else {
                _num_containers = (s / CONTAINER_SIZE);
            }

            // set the max capacity of the outer arrays
            _capacity = _num_containers * CONTAINER_SIZE;

            // allocate a fat chunk
            _outer_start = _outer_array_allocator.allocate(_num_containers);

            for(int i = 0; i < _num_containers; ++i) {
                if(i == 0) {
                    _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
                    _outer_start[0] = _used_start;
                } else {
                    _outer_start[i] = _inner_array_allocator.allocate(CONTAINER_SIZE);
                }
            }
            _used_end = _used_start + _size;

            _outer_end = _outer_start + _num_containers;
            _outer_used_start = _outer_start;
            _outer_used_end = _outer_end;

            // fill
            my_uninitialized_fill(_inner_array_allocator, begin(), end(), v);

        } else {
            throw std::out_of_range("Please input a valid size for deque.");
        }
        assert(valid());
    }

    /**
     * construct and duplicate v, using allocator type a
     */
    my_deque (size_type s, const_reference v, const allocator_type& a) : _inner_array_allocator(a) {
        if(s == 0) {
            // allocate one block
            _outer_start = _outer_array_allocator.allocate(1);
            _outer_end = _outer_start + 1;
            _outer_used_end = _outer_end;
            _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
            _outer_start[0] = _used_start;
            _outer_used_start = _outer_start;
            _used_end = _used_start;
            _size = 0;
            _num_containers = 1;
            _capacity = CONTAINER_SIZE;
        } else if(s > 0) {
            // set size
            _size = s;

            // set num_containers
            if((s % CONTAINER_SIZE) > 0) { // indicates that we have a partial container
                _num_containers = (s / CONTAINER_SIZE) + 1;
            } else {
                _num_containers = (s / CONTAINER_SIZE);
            }

            // set the max capacity of the outer arrays
            _capacity = _num_containers * CONTAINER_SIZE;

            // allocate a fat chunk
            _outer_start = _outer_array_allocator.allocate(_num_containers);

            for(int i = 0; i < _num_containers; ++i) {
                if(i == 0) {
                    _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
                    _outer_start[0] = _used_start;
                } else {
                    _outer_start[i] = _inner_array_allocator.allocate(CONTAINER_SIZE);
                }
            }
            _used_end = _used_start + _size;

            _outer_end = _outer_start + _num_containers;
            _outer_used_start = _outer_start;
            _outer_used_end = _outer_end;

            // fill
            my_uninitialized_fill(_inner_array_allocator, begin(), end(), v);

        } else {
            throw std::out_of_range("Please input a valid size for deque.");
        }
        assert(valid());
    }

    /**
     * copy a list and construct the deque to contain said list
     */
    my_deque (std::initializer_list<value_type> rhs) { // use copy
        size_type s = rhs.size();
        if(s == 0) {
            // allocate one block
            _outer_start = _outer_array_allocator.allocate(1);
            _outer_end = _outer_start + 1;
            _outer_used_end = _outer_end;
            _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
            _outer_start[0] = _used_start;
            _outer_used_start = _outer_start;
            _used_end = _used_start;
            _size = 0;
            _num_containers = 1;
            _capacity = CONTAINER_SIZE;
        } else if(s > 0) {
            // set size
            _size = s;

            // set num_containers
            if((s % CONTAINER_SIZE) > 0) { // indicates that we have a partial container
                _num_containers = (s / CONTAINER_SIZE) + 1;
            } else {
                _num_containers = (s / CONTAINER_SIZE);
            }

            // set the max capacity of the outer arrays
            _capacity = _num_containers * CONTAINER_SIZE;

            // allocate a fat chunk
            _outer_start = _outer_array_allocator.allocate(_num_containers);

            for(int i = 0; i < _num_containers; ++i) {
                if(i == 0) {
                    _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
                    _outer_start[0] = _used_start;
                } else {
                    _outer_start[i] = _inner_array_allocator.allocate(CONTAINER_SIZE);
                }
            }
            _used_end = _used_start + _size;

            _outer_end = _outer_start + _num_containers;
            _outer_used_start = _outer_start;
            _outer_used_end = _outer_end;

            // copy
            my_uninitialized_copy(_inner_array_allocator, rhs.begin(), rhs.end(), begin());

        } else {
            throw std::out_of_range("Please input a valid size for deque.");
        }

        assert(valid());
    }

    /**
     * construct deque from list and given allocator type a
     */
    my_deque (std::initializer_list<value_type> rhs, const allocator_type& a) : _inner_array_allocator(a) {
        size_type s = rhs.size();
        if(s == 0) {
            // allocate one block
            _outer_start = _outer_array_allocator.allocate(1);
            _outer_end = _outer_start + 1;
            _outer_used_end = _outer_end;
            _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
            _outer_start[0] = _used_start;
            _outer_used_start = _outer_start;
            _used_end = _used_start;
            _size = 0;
            _num_containers = 1;
            _capacity = CONTAINER_SIZE;
        } else if(s > 0) {
            // set size
            _size = s;

            // set num_containers
            if((s % CONTAINER_SIZE) > 0) { // indicates that we have a partial container
                _num_containers = (s / CONTAINER_SIZE) + 1;
            } else {
                _num_containers = (s / CONTAINER_SIZE);
            }

            // set the max capacity of the outer arrays
            _capacity = _num_containers * CONTAINER_SIZE;

            // allocate a fat chunk
            _outer_start = _outer_array_allocator.allocate(_num_containers);

            for(int i = 0; i < _num_containers; ++i) {
                if(i == 0) {
                    _used_start = _inner_array_allocator.allocate(CONTAINER_SIZE);
                    _outer_start[0] = _used_start;
                } else {
                    _outer_start[i] = _inner_array_allocator.allocate(CONTAINER_SIZE);
                }
            }
            _used_end = _used_start + _size;

            _outer_end = _outer_start + _num_containers;
            _outer_used_start = _outer_start;
            _outer_used_end = _outer_end;

            // copy
            my_uninitialized_copy(_inner_array_allocator, rhs.begin(), rhs.end(), begin());

        } else {
            throw std::out_of_range("Please input a valid size for deque.");
        }

        assert(valid());
    }

    /**
     * create a deque given a reference to another deque
     */
    my_deque (const my_deque& that) {
        // clear all state variables
        //_outer_array_allocator = 0;
        //_inner_array_allocator = 0;
        _outer_start = that._outer_start;
        _outer_end = that._outer_end;
        _outer_used_start = that._outer_used_start;
        _outer_used_end = that._outer_used_end;
        _used_start = that._used_start;
        _used_end = that._used_end;
        _size = that._size;
        _capacity = that._capacity;

        assert(valid());
    }

    // ----------
    // destructor
    // ----------

    /**
     * destructor for my deque, clear it
     */
    /*
            ~my_vector () {
                if (!empty()) {
                    clear();
                    _a.deallocate(_b, capacity());}
                assert(valid());}
    */

    ~my_deque () {
        clear();
        // resize should do this but just in case
        //my_destroy(_inner_array_allocator, begin(), end());
        // for(int i = _num_containers - 1; i >= 0; --i){
        //     _inner_array_allocator.deallocate(_outer_start[i], CONTAINER_SIZE);
        // }
        // _outer_array_allocator.deallocate(_outer_start, _num_containers);
        assert(valid());
    }

    // ----------
    // operator =
    // ----------

    /**
     * assignment operator for deque
     */
    my_deque& operator = (const my_deque& rhs) {
        if(*this == rhs) {
            return *this;
        }
        if((*this).size() == rhs.size()) {
            std::copy(rhs.begin(), rhs.end(), begin());
            resize(rhs.size());
        } else {
            clear();
            resize(rhs.size());
            auto tail = std::copy(rhs.begin(), rhs.end(), begin());
            _used_end = &(*tail);
        }
        // <your code>
        assert(valid());
        return *this;
    }

    // -----------
    // operator []
    // -----------

    /**
     * retrieve value from a given index in deque
     */
    reference operator [] (size_type index) {
        size_type start_remainder = 0;
        pointer start = _used_start;
        pointer end = *_outer_used_start + CONTAINER_SIZE - 1;
        while(start <= end) {
            ++start_remainder;
            ++start;
        }
        if(index < start_remainder) {
            return *(_used_start + index);
        }
        else {
            size_type outer_idx = (index - start_remainder) / CONTAINER_SIZE + 1;
            size_type inner_idx = (index - start_remainder) % CONTAINER_SIZE;
            pointer* o_start = _outer_used_start + outer_idx;
            return *(*o_start + inner_idx);
        }

        // size_type offset = _used_end - *_outer_used_start;
        // size_type outer_idx = (index + offset) / CONTAINER_SIZE;
        // size_type inner_idx = (index + offset) % CONTAINER_SIZE;
        //int[] start_container = ;
        // if index > (number of elements in first container)
        // (index - # elements in first container) / CONTAINER_SIZE = outer array index to search in (var x)
        // outer array[x] -> (index - # elements in first container) % CONTAINER_SIZE = value target
        // static value_type dummy; // remove
        // return (_outer_start + outer_idx);
    }

    /**
     * const version of [] operator
     */
    const_reference operator [] (size_type index) const {
        return const_cast<my_deque*>(this)->operator[](index);
    }

    // --
    // at
    // --

    /**
     * retrieve value at index
     * @throws out_of_range
     */
    reference at (size_type index) {
        if(index < 0 || index > (_size - 1)) {
            throw std::out_of_range("Index is out of range for deque.");
        }
        return (*this)[index];
    }

    /**
     * const version of at
     * @throws out_of_range
     */
    const_reference at (size_type index) const {
        return const_cast<my_deque*>(this)->at(index);
    }

    // ----
    // back
    // ----

    /**
     * return backmost element
     */
    reference back () {
        // <your code>
        return this->operator[](_size - 1);
    }           // fix

    /**
     * return backmost elemnt
     */
    const_reference back () const {
        return const_cast<my_deque*>(this)->back();
    }

    // -----
    // begin
    // -----

    /**
     * return first element
     */
    iterator begin () {
        return iterator(this, 0);
    }

    /**
     * return first element
     */
    const_iterator begin () const {
        return const_iterator(this);
    }

    // -----
    // clear
    // -----

    /**
     * destroy entire deque
     */
    void clear () {
        // destroys in resize
        resize(0);
        assert(valid());
    }

    // -----
    // empty
    // -----

    /**
     * returns whether deque is empty
     */
    bool empty () const {
        return !size();
    }

    // ---
    // end
    // ---

    /**
     * returns iterator to end of deque
     */
    iterator end () {
        return iterator(this, _size);
    }

    /**
     * returne iterator to end of ddeque
     */
    const_iterator end () const {
        return const_iterator(this, _size);
    }

    // -----
    // erase
    // -----

    /**
     * removes an element from deque
     */
    iterator erase (iterator i) {
        auto inital = i;
        if(i == end() - 1) {
            pop_back();
        }
        else {
            std::copy(i + 1, end(), i);
            resize(_size - 1);
            assert(valid());
        }
        return inital;
    }

    // -----
    // front
    // -----

    /**
     * returns the front of the dequeeturns front of deque (first element)
     */
    reference front () {
        return this->operator[](0);
    }

    /**
     * returns the front of the const dequefirst elem
     */
    const_reference front () const {
        return const_cast<my_deque*>(this)->front();
    }

    // ------
    // insert
    // ------

    /**
     * inserts a value v into the deque given interator i
     */
    iterator insert (iterator i, const_reference v) {
        auto b = begin();
        auto e = end();
        if(i == b) {
            push_front(v);
            return begin();
        }
        if(i == e) {
            push_back(v);
            return end();
        } else {
            resize(_size + 1);
            for(auto new_e = end(); (new_e - 1) != i; --new_e) {
                *new_e = *(new_e - 1);
            }
            *i = v;
            return i;
        }

        assert(valid());
        //return iterator();
    }

    // ---
    // pop
    // ---

    /**
     * pops a value off of the back of the deque
     */
    void pop_back () {
        if(_size == 0) {
            throw std::out_of_range("No elements in deque to pop.");
        }
        resize(_size - 1);
        assert(valid());
    }

    /**
     * pops a value off of the front of the deque
     */
    void pop_front () {
        if(_size == 0) {
            throw std::out_of_range("No elements in deque to pop.");
        }
        _inner_array_allocator.destroy(_used_start);
        _used_start = &at(1);
        --_size;
        assert(valid());
    }

    // ----
    // push
    // ----

    /**
     * pushes a value v to the back of the deque
     */
    void push_back (const_reference v) {
        resize(size() + 1, v);
        assert(valid());
    }

    /**
     * pushes a value v to the front of the deque
     */
    void push_front (const_reference v) {
        // <your code>
        if(_used_start != *_outer_used_start) {
            --_used_start;
            *_used_start = v;
            ++_size;
        } else if (_outer_used_start != _outer_start) { // there are remaining containers at the front of the outer array
            --_outer_used_start;
            _used_start = *_outer_used_start + CONTAINER_SIZE - 1;
            *_used_start = v;
            ++_size;
        }
        else {
            // must reallocate new outer array and transfer old pointers
            size_type blocks_to_add = 1;
            _capacity += (blocks_to_add) * CONTAINER_SIZE;
            ++_size;
            _num_containers += blocks_to_add;
            // allocate new array
            pointer* _outer_start_new = _outer_array_allocator.allocate(_num_containers);
            pointer* _outer_used_start_new = _outer_start_new;
            // one past the last index of outer array
            pointer* _outer_end_new = _outer_start_new + _capacity / CONTAINER_SIZE;
            pointer* _outer_used_end_new = _outer_end_new - (_outer_end - _outer_used_end);

            // copy over old pointers
            pointer* temp_new = _outer_start_new + 1;
            while(_outer_start < _outer_end) {
                *temp_new = *_outer_start;
                ++temp_new;
                ++_outer_start;
            }
            // set used_start and used_end to the new copied pointers again
            //pointer _used_start_new = *_outer_used_start_new;

            // replace new variables as current private instance variables
            _outer_start = _outer_start_new;
            _outer_end = _outer_end_new;
            _outer_used_start = _outer_used_start_new;
            _outer_used_end = _outer_used_end_new;
            //_used_start = _used_start_new;

            // allocate arrays to the new containers added at end of outer array
            *_outer_start = _inner_array_allocator.allocate(CONTAINER_SIZE);


            // pointer _used_end_new = *(_outer_used_end - 1) + ((_size - 1) % CONTAINER_SIZE);
            // _used_end = _used_end_new;

            // set front of deque to v
            _used_start = *_outer_used_start + CONTAINER_SIZE - 1;
            *_used_start = v;
        }
        assert(valid());
    }

    // ------
    // resize
    // ------

    /**
     * give a size s , this resizes the deque to that size s
     */
    void resize (size_type s) {
        // <your code>
        if(s == _size) {
            return;
        } else if (s < _size) { // incomplete PROB HAVE TO FIX?
            auto b = begin();
            for(int i = 0; i < s; i++)
                ++b;
            auto tail = my_destroy(_inner_array_allocator, b, end());
            _size = s;
            _used_end = &*tail;
            _outer_used_end = _outer_used_start + _size / CONTAINER_SIZE; // maybe off by one
        } else {
            size_type outer_end_offset = _outer_end - _outer_used_end; // maybe off by one
            size_type inner_end_offset = (*(_outer_used_end - 1) + CONTAINER_SIZE) - _used_end;
            size_type remaining_end = inner_end_offset + outer_end_offset * CONTAINER_SIZE;
            if((s - _size) <= remaining_end) {
                // copy into end of deque
                auto b = begin();
                for(int i = 0; i < _size; i++)
                    ++b;
                auto e = end();
                for(int j = 0; j < (s - _size); j++)
                    ++e;
                my_uninitialized_fill(_inner_array_allocator, b, e, value_type());
                _used_end += (s - _size);
                _size = s;
            }
            else {
                // must reallocate new outer array and transfer old pointers
                size_type blocks_to_add = ((s - _size) - remaining_end - 1) / CONTAINER_SIZE + 1;
                _capacity += (blocks_to_add) * CONTAINER_SIZE;
                size_type old_size = _size;
                _size = s;
                _num_containers += blocks_to_add;

                //allocate new array
                pointer* _outer_start_new = _outer_array_allocator.allocate(_num_containers);
                pointer* _outer_used_start_new = _outer_start_new + (_outer_used_start - _outer_start);
                // one past the last index of outer array
                pointer* _outer_end_new = _outer_start_new + _capacity / CONTAINER_SIZE;
                pointer* _outer_used_end_new = _outer_start_new + old_size / CONTAINER_SIZE;

                //copy over old pointers
                pointer* temp_new = _outer_start_new;
                while(_outer_start < _outer_end) {
                    *temp_new = *_outer_start;
                    ++temp_new;
                    ++_outer_start;
                }

                // set used_start and used_end to the new copied pointers again
                //pointer _used_start_new = *_outer_used_start_new;

                // replace new variables as current private instance variables
                _outer_start = _outer_start_new;
                _outer_end = _outer_end_new;
                _outer_used_start = _outer_used_start_new;
                _outer_used_end = _outer_used_end_new;
                //_used_start = _used_start_new;

                // allocate arrays to the new containers added at end of outer array
                for(int i = 0; i < blocks_to_add; i++) {
                    *_outer_used_end = _inner_array_allocator.allocate(CONTAINER_SIZE);
                    ++_outer_used_end;
                }

                pointer _used_end_new = *(_outer_used_end - 1) + ((_size - 1) % CONTAINER_SIZE);
                _used_end = _used_end_new;


                // copy into end of deque
                auto b = begin();
                for(int i = 0; i < _size; i++)
                    ++b;
                auto e = end();
                for(int j = 0; j < (s - _size); j++)
                    ++e;
                my_uninitialized_fill(_inner_array_allocator, b, e, value_type());
            }
        }

        assert(valid());

    }

    /**
     * give a size s and a value v, this resizes the deque to that size s and contains that value v
     */
    void resize (size_type s, const_reference v) {
        // <your code>
        if(s == _size) {
            return;
        } else if (s < _size) { // incomplete PROB HAVE TO FIX?
            auto b = begin();
            for(int i = 0; i < s; i++)
                ++b;
            auto tail = my_destroy(_inner_array_allocator, b, end());
            _size = s;
            _used_end = &*tail;
            _outer_used_end = _outer_used_start + _size / CONTAINER_SIZE; // maybe off by one
        } else {
            size_type outer_end_offset = _outer_end - _outer_used_end; // maybe off by one
            size_type inner_end_offset = (*(_outer_used_end - 1) + CONTAINER_SIZE) - _used_end;
            size_type remaining_end = inner_end_offset + outer_end_offset * CONTAINER_SIZE;
            if((s - _size) <= remaining_end) {
                // copy into end of deque
                auto b = begin();
                for(int i = 0; i < _size; i++)
                    ++b;
                auto e = end();
                for(int j = 0; j < (s - _size); j++)
                    ++e;
                my_uninitialized_fill(_inner_array_allocator, b, e, v);
                _used_end += (s - _size);
                _size = s;
            }
            else {
                // must reallocate new outer array and transfer old pointers
                size_type blocks_to_add = ((s - _size) - remaining_end - 1) / CONTAINER_SIZE + 1;
                _capacity += (blocks_to_add) * CONTAINER_SIZE;
                size_type old_size = _size;
                _size = s;
                _num_containers += blocks_to_add;

                //allocate new array
                pointer* _outer_start_new = _outer_array_allocator.allocate(_num_containers);
                pointer* _outer_used_start_new = _outer_start_new + (_outer_used_start - _outer_start);
                // one past the last index of outer array
                pointer* _outer_end_new = _outer_start_new + _capacity / CONTAINER_SIZE;
                pointer* _outer_used_end_new = _outer_start_new + old_size / CONTAINER_SIZE;

                //copy over old pointers
                pointer* temp_new = _outer_start_new;
                while(_outer_start < _outer_end) {
                    *temp_new = *_outer_start;
                    ++temp_new;
                    ++_outer_start;
                }

                // set used_start and used_end to the new copied pointers again
                //pointer _used_start_new = *_outer_used_start_new;

                // replace new variables as current private instance variables
                _outer_start = _outer_start_new;
                _outer_end = _outer_end_new;
                _outer_used_start = _outer_used_start_new;
                _outer_used_end = _outer_used_end_new;
                //_used_start = _used_start_new;

                // allocate arrays to the new containers added at end of outer array
                for(int i = 0; i < blocks_to_add; i++) {
                    *_outer_used_end = _inner_array_allocator.allocate(CONTAINER_SIZE);
                    ++_outer_used_end;
                }

                pointer _used_end_new = *(_outer_used_end - 1) + ((_size - 1) % CONTAINER_SIZE);
                _used_end = _used_end_new;


                // copy into end of deque
                auto b = begin();
                for(int i = 0; i < _size; i++)
                    ++b;
                auto e = end();
                for(int j = 0; j < (s - _size); j++)
                    ++e;
                my_uninitialized_fill(_inner_array_allocator, b, e, v);
            }
        }

        assert(valid());
    }

    // ----
    // size
    // ----

    /**
     * returns the size of the deque
     */
    size_type size () const {
        return _size;
    }

    // ----
    // swap
    // ----

    /**
     * given a deque d, we swap its contents/parameters with "this"
     */
    void swap (my_deque& d) {
        std::swap(this->_outer_array_allocator, d._outer_array_allocator);
        std::swap(this->_inner_array_allocator, d._inner_array_allocator);
        std::swap(this->_outer_start, d._outer_start);
        std::swap(this->_outer_end, d._outer_end);
        std::swap(this->_outer_used_start, d._outer_used_start);
        std::swap(this->_outer_used_end, d._outer_used_end);
        std::swap(this->_used_start, d._used_start);
        std::swap(this->_used_end, d._used_end);
        std::swap(this->_size, d._size);
        std::swap(this->_capacity, d._capacity);
        assert(valid());
    }
};

#endif // Deque_h
