# CS371g: Generic Programming Deque Repo

* Name: Srinidhi Krishnamurthy and Steven Sun

* EID: spk642, srs4698

* GitLab ID: srikrishnamurthy, ssun914

* Git SHA: 6e54264461b11778c78d830155251452f9138d8d

* GitLab Pipelines: https://gitlab.com/ssun914/cs371g-deque/-/pipelines

* Estimated completion time: 15 hrs

* Actual completion time: 30 hrs

* Comments:
